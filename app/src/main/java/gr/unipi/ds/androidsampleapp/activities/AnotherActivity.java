package gr.unipi.ds.androidsampleapp.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import gr.unipi.ds.androidsampleapp.R;

public class AnotherActivity extends AppCompatActivity {
    public static final String EXTRAS_KEY_NAME = "name_key";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_another);

        String name = getIntent().getStringExtra(EXTRAS_KEY_NAME);
        TextView nameTextView = findViewById(R.id.txt_activity_another_name);
        nameTextView.setText(name);
    }
}
