package gr.unipi.ds.androidsampleapp.activities;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.webkit.URLUtil;
import android.widget.TextView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import gr.unipi.ds.androidsampleapp.R;

public class AsyncTaskActivity extends AppCompatActivity {
    private static final String TAG = AsyncTaskActivity.class.getSimpleName();

    public static final String EXTRAS_URL = "EXTRAS_URL";

    private TextView mResultTextView;

    //private static final String URL_ADDRESS = "http://www.unipi.gr";
    private static final String FILE_NAME = "content.txt";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_async_task);

        mResultTextView = findViewById(R.id.txt_activity_async_task_result);

        if (getIntent() != null && getIntent().getExtras() != null) {
            String txt = getIntent().getExtras().getString(EXTRAS_URL);
            mResultTextView.setText(txt);

            if (URLUtil.isHttpUrl(txt) || URLUtil.isHttpsUrl(txt)) {
                new DownloadFromUrl().execute(txt);
            }
        }
    }

    /**
     * Stores data in an internal txt file.
     *
     * @param fileName The name of the file.
     * @param content  The content to be saved into the file.
     */
    private void storeInternal(String fileName, String content) {
        try (FileOutputStream outputStream = openFileOutput(fileName, Context.MODE_PRIVATE)) {
            outputStream.write(content.getBytes());
        } catch (Exception e) {
            Log.e(TAG, "Exception Error: " + e.getMessage());
        }
    }

    /**
     * Stores data in an external txt file.
     *
     * @param fileName The name of the file.
     * @param content  The content to be saved into the file.
     */
    private void storeExternal(String fileName, String content) {
        String state = Environment.getExternalStorageState();
        if (!Environment.MEDIA_MOUNTED.equals(state)) {
            Log.e(TAG, "Failed to write in the external storage.");
            return;
        }

        File file = new File(Environment.getExternalStorageDirectory() + "/" + fileName);

        try (FileOutputStream outputStream = new FileOutputStream(file)) {
            outputStream.write(content.getBytes());
        } catch (Exception e) {
            Log.e(TAG, "Exception Error: " + e.getMessage());
        }
    }

    /**
     * This class downloads the html content of a web site to demonstrate the use of {@link AsyncTask}.
     */
    private class DownloadFromUrl extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            StringBuilder sb = new StringBuilder();
            try {
                URL url = new URL(params[0]);
                InputStream input = url.openStream();
                int c;
                while ((c = input.read()) > -1) {
                    sb.append((char) c);
                }
                input.close();
            } catch (MalformedURLException e) {
                Log.e(TAG, "Wrong url");
            } catch (IOException e) {
                Log.e(TAG, "IOException Error: " + e.getMessage());
            }

            storeInternal(FILE_NAME, sb.toString());
            storeExternal(FILE_NAME, sb.toString());

            return sb.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            Log.i(TAG, "Download has finished");
            mResultTextView.setText(result);
        }
    }
}
