package gr.unipi.ds.androidsampleapp.activities;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.List;

import gr.unipi.ds.androidsampleapp.R;
import gr.unipi.ds.androidsampleapp.models.Pet;
import gr.unipi.ds.androidsampleapp.models.User;
import gr.unipi.ds.androidsampleapp.retrofit.PetStoreApi;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();

    private static final int REQUEST_IMAGE_CAPTURE = 1;
    private static final int REQUEST_PERMISSIONS = 2;

    private static final String PREFS_NAME = "SP";
    private static final String PREFS_KEY_1 = "PREFS_KEY_1";

    private TextView mTextView;
    private Button mButton;
    private ImageView mImageView;
    private SharedPreferences sp;

    private PetStoreApi mPetStoreApi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreate");
        setContentView(R.layout.activity_main);

        mTextView = findViewById(R.id.txt_activity_main_title);
        mButton = findViewById(R.id.btn_activity_main_action);
        mImageView = findViewById(R.id.img_activity_main);

        mTextView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                goToAsyncTask(null);
                return false;
            }
        });

        sp = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://petstore.swagger.io/v2/")
                .addConverterFactory(GsonConverterFactory.create()) //Εάν θέλουμε GSON
                .build();
        mPetStoreApi = retrofit.create(PetStoreApi.class);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(TAG, "onResume");

        String txt = sp.getString(PREFS_KEY_1, "");
        mTextView.setText(txt);
    }

    @Override
    protected void onPause() {
        Log.i(TAG, "onPause");

        String txt = mTextView.getText().toString();
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(PREFS_KEY_1, txt);
        editor.commit();

        super.onPause();
    }

    @Override
    protected void onDestroy() {
        Log.i(TAG, "onDestroy");
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.create_new:
                Log.i(TAG, "Just pressed create_new.");
                Toast.makeText(this, "Just pressed create_new.", Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(this, AnotherActivity.class);
                intent.putExtra(AnotherActivity.EXTRAS_KEY_NAME, "Andreas");
                startActivity(intent);

                return true;
            case R.id.open:
                Log.i(TAG, "Just pressed open.");
                dispatchTakePictureIntent();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.dialog_message)
                .setTitle(R.string.dialog_title)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            mImageView.setImageBitmap(imageBitmap);
            mImageView.setVisibility(View.VISIBLE);
        }
    }

    public void goToAsyncTask(View view) {
        Log.i(TAG, "goToAsyncTask!");
        String txt = mTextView.getText().toString();
        Intent intent = new Intent(this, RealmActivity.class);
        intent.putExtra(AsyncTaskActivity.EXTRAS_URL, txt);
        startActivity(intent);
    }

    private void checkPermissions() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return;
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed; request the permission
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        REQUEST_PERMISSIONS);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {
            // Permission has already been granted
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSIONS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }


    public void loadUser(View view) {
        mPetStoreApi.getUser("string").enqueue(
                new Callback<User>() {
                    @Override
                    public void onResponse(Call<User> call, Response<User> response) {
                        if (response.isSuccessful()) {
                            User user = response.body();
                            mTextView.setText("I am user: " + user.getUsername());
                        }
                    }

                    @Override
                    public void onFailure(Call<User> call, Throwable t) {
                        mTextView.setText("Request failed");
                    }
                });
    }

    public void loadPet(View view) {
        mPetStoreApi.getPet(2).enqueue(
                new Callback<Pet>() {
                    @Override
                    public void onResponse(Call<Pet> call, Response<Pet> response) {
                        if (response.isSuccessful()) {
                            Pet pet = response.body();
                            mTextView.setText("I am pet: " + pet.getName());
                        }
                    }

                    @Override
                    public void onFailure(Call<Pet> call, Throwable t) {
                        mTextView.setText("Request failed");
                    }
                });
    }

    public void findByStatus(View view) {
        mPetStoreApi.findByStatus("available").enqueue(new Callback<List<Pet>>() {
            @Override
            public void onResponse(Call<List<Pet>> call, Response<List<Pet>> response) {
                if (response.isSuccessful()) {
                    List<Pet> pets = response.body();
                    Log.i(TAG, "I have downloaded " + pets.size() + " pets");
                    for(Pet pet: pets){
                        Log.i(TAG, pet.getId() + " - " + pet.getName());
                    }
                } else {
                    try {
                        Log.e(TAG, "failed: " + response.code() + " / " + response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<List<Pet>> call, Throwable t) {
                Log.e(TAG, t.getMessage());
            }
        });
    }
}
