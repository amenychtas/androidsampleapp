package gr.unipi.ds.androidsampleapp.activities;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import gr.unipi.ds.androidsampleapp.R;
import gr.unipi.ds.androidsampleapp.models.Pet;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;

public class PetAdapter extends BaseAdapter {
    private final Activity activity;
    private final RealmResults<Pet> pets;

    PetAdapter(@NonNull Activity activity, @NonNull RealmResults<Pet> pets) {
        this.activity = activity;
        this.pets = pets;

        pets.addChangeListener(new RealmChangeListener<RealmResults<Pet>>() {
            @Override
            public void onChange(RealmResults<Pet> pets) {
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getCount() {
        return pets.size();
    }

    @Override
    public Object getItem(int position) {
        return pets.get(position);
    }

    @Override
    public long getItemId(int position) {
        return pets.get(position).hashCode();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Pet pet = pets.get(position);

        final PetsHolder holder;
        View row = convertView;

        if (row == null) {
            LayoutInflater inflater = activity.getLayoutInflater();
            row = inflater.inflate(R.layout.listview_item_pet, parent, false);

            holder = new PetsHolder();
            holder.idTextView = row.findViewById(R.id.txt_listview_item_pet_id);
            holder.statusTextView = row.findViewById(R.id.txt_listview_item_pet_status);
            holder.nameTextView = row.findViewById(R.id.txt_listview_item_pet_name);
            row.setTag(holder);
        } else {
            holder = (PetsHolder) row.getTag();
        }

        holder.idTextView.setText(String.valueOf(pet.getId()));
        holder.statusTextView.setText(pet.getStatus());
        holder.nameTextView.setText(pet.getName());

        return row;
    }

    private class PetsHolder {
        TextView idTextView;
        TextView statusTextView;
        TextView nameTextView;
    }
}
