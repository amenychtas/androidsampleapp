package gr.unipi.ds.androidsampleapp.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

import gr.unipi.ds.androidsampleapp.R;
import gr.unipi.ds.androidsampleapp.models.Pet;
import gr.unipi.ds.androidsampleapp.retrofit.PetStoreApi;
import gr.unipi.ds.androidsampleapp.utils.G;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RealmActivity extends AppCompatActivity {
    private Realm realm;
    private PetStoreApi petstoreApi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_realm);

        realm = Realm.getDefaultInstance();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(G.PETSTORE_HOST)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        petstoreApi = retrofit.create(PetStoreApi.class);

        final RealmResults<Pet> pets = realm.where(Pet.class).findAll().sort("id", Sort.ASCENDING);
        PetAdapter petAdapter = new PetAdapter(this, pets);
        ListView listView = findViewById(R.id.list_activity_realm);
        listView.setAdapter(petAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Pet pet = pets.get(position);

                realm.beginTransaction();
                pet.deleteFromRealm();
                realm.commitTransaction();
            }
        });
    }


    @Override
    protected void onDestroy() {
        realm.close();

        super.onDestroy();
    }

    public void getAvailable(View view) {
        getPets("available");
    }

    public void getPending(View view) {
        getPets("pending");
    }

    public void getSold(View view) {
        getPets("sold");
    }

    private void getPets(String status) {
        petstoreApi.findByStatus(status).enqueue(new Callback<List<Pet>>() {
            @Override
            public void onResponse(Call<List<Pet>> call, Response<List<Pet>> response) {
                if (response.isSuccessful() && response.body() != null) {
                    List<Pet> pets = response.body();

                    realm.beginTransaction();
                    realm.delete(Pet.class);
                    realm.copyToRealmOrUpdate(pets);
                    realm.commitTransaction();

                    Toast.makeText(RealmActivity.this, pets.size() + " pets loaded", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(RealmActivity.this, "Error loading pets", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<Pet>> call, Throwable t) {
                Toast.makeText(RealmActivity.this, "Error loading pets", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
