package gr.unipi.ds.androidsampleapp.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import gr.unipi.ds.androidsampleapp.R;

public class SendToActivity extends AppCompatActivity {
    private static final String TAG = SendToActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_to);

        // Get the intent that started this activity
        Intent intent = getIntent();
        if (intent == null || intent.getType() == null) {
            return;
        }

        // Figure out what to do based on the intent type
        if (intent.getType().contains("image/")) {
            Uri imageUri = intent.getParcelableExtra(Intent.EXTRA_STREAM);
            ImageView img = findViewById(R.id.img_activity_sent_to);
            img.setImageURI(imageUri);

        } else if (intent.getType().equals("text/plain")) {
            TextView txt = findViewById(R.id.txt_activity_sent_to);
            txt.setText(intent.getStringExtra(Intent.EXTRA_TEXT));
        }
    }
}
