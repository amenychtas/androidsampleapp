package gr.unipi.ds.androidsampleapp.activities;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import gr.unipi.ds.androidsampleapp.R;

public class SensorsActivity extends AppCompatActivity implements SensorEventListener {
    private SensorManager mSensorManager;

    private TextView txtX;
    private TextView txtY;
    private TextView txtZ;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sensors);
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

        txtX = findViewById(R.id.txt_activity_sensors_x);
        txtY = findViewById(R.id.txt_activity_sensors_y);
        txtZ = findViewById(R.id.txt_activity_sensors_z);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    @Override
    protected void onResume() {
        super.onResume();
        Sensor sensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mSensorManager.registerListener(this,
                sensor,
                SensorManager.SENSOR_DELAY_NORMAL,
                SensorManager.SENSOR_DELAY_UI);
    }

    @Override
    protected void onPause() {
        mSensorManager.unregisterListener(this);
        super.onPause();
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        txtX.setText(String.valueOf(event.values[0]));
        txtY.setText(String.valueOf(event.values[1]));
        txtZ.setText(String.valueOf(event.values[2]));
    }
}
