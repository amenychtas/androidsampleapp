package gr.unipi.ds.androidsampleapp.activities;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import gr.unipi.ds.androidsampleapp.R;
import gr.unipi.ds.androidsampleapp.receivers.BatteryReceiver;
import gr.unipi.ds.androidsampleapp.services.MyService;

public class TestServicesActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();

    private TextView mTextView;
    private Button mButton;

    private CountDownTimer countDownTimer;
    private int NOTIFICATION_ID = 123;

    private BatteryReceiver br;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_services);

        mTextView = findViewById(R.id.txt_activity_main_title);
        mButton = findViewById(R.id.btn_activity_main_action);

        mTextView.setOnClickListener(v -> {
            mTextView.setText(null);
            //setTimer(true);
        });

        //showNotification();

        br = new BatteryReceiver();

        Intent serviceIntent = new Intent(this, MyService.class);
        startService(serviceIntent);
    }

    @Override
    protected void onResume() {
        super.onResume();
//        IntentFilter filter = new IntentFilter();
//        filter.addAction(Intent.ACTION_POWER_CONNECTED);
//        filter.addAction(Intent.ACTION_POWER_DISCONNECTED);
//        registerReceiver(br, filter);
    }

    @Override
    protected void onPause() {
        setTimer(false);
//        unregisterReceiver(br);
        super.onPause();
    }

    public void startActivity(View view) {
        Intent intent = new Intent(this, ListActivity.class);
        startActivity(intent);
    }

    private void setTimer(boolean enable) {
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }

        if (enable) {
            showNotification();
            countDownTimer = new CountDownTimer(10000, 1000) {
                public void onTick(long millisUntilFinished) {
                    //Do something
                    mTextView.setText(millisUntilFinished / 1000 + " seconds");
                }

                public void onFinish() {
                    //finish activity
                    NotificationManager mNotificationManager =
                            (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    mNotificationManager.cancel(NOTIFICATION_ID);
                }
            }.start();
        }
    }

    private void showNotification() {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_folder_open_black_24dp)
                        .setContentTitle("My Notification")
                        .setContentText("Hello World!")
                        .setAutoCancel(true);
        Intent resultIntent = new Intent(this, ListActivity.class);
        PendingIntent resultPendingIntent = PendingIntent.getActivity(
                this,
                0,
                resultIntent,
                PendingIntent.FLAG_UPDATE_CURRENT
        );
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }
}
