package gr.unipi.ds.androidsampleapp.retrofit;

import java.util.List;

import gr.unipi.ds.androidsampleapp.models.Pet;
import gr.unipi.ds.androidsampleapp.models.User;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by amenychtas on 19/03/2018.
 */

public interface PetStoreApi {

    @GET("user/{username}")
    Call<User> getUser(@Path("username") String username);

    @GET("pet/{id}")
    Call<Pet> getPet(@Path("id") long id);

    @GET("pet/findByStatus")
    Call<List<Pet>> findByStatus(@Query("status") String status);
}
