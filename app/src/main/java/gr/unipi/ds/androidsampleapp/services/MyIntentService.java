package gr.unipi.ds.androidsampleapp.services;

import android.app.IntentService;
import android.content.Intent;


public class MyIntentService extends IntentService {
    public static final String ACTION_FOO = "gr.unipi.androidsampleapp.action.FOO";
    public static final String ACTION_BAR = "gr.unipi.androidsampleapp.action.BAR";

    public static final String EXTRA_PARAM1 = "gr.unipi.androidsampleapp.extra.PARAM1";
    public static final String EXTRA_PARAM2 = "gr.unipi.androidsampleapp.extra.PARAM2";

    public MyIntentService() {
        super("MyIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_FOO.equals(action)) {
                final String param1 = intent.getStringExtra(EXTRA_PARAM1);
                final String param2 = intent.getStringExtra(EXTRA_PARAM2);
                handleActionFoo(param1, param2);
            } else if (ACTION_BAR.equals(action)) {
                final String param1 = intent.getStringExtra(EXTRA_PARAM1);
                final String param2 = intent.getStringExtra(EXTRA_PARAM2);
                handleActionBaz(param1, param2);
            }
        }
    }

    private void handleActionFoo(String param1, String param2) {
        // TODO: Handle action Foo
        throw new UnsupportedOperationException("Not yet implemented");
    }

    private void handleActionBaz(String param1, String param2) {
        // TODO: Handle action Baz
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
