package gr.unipi.ds.androidsampleapp.services;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.util.Log;

public class MyService extends Service {
    private static final String TAG = MyService.class.getSimpleName();
    private static final int MY_MSG = 1;

    private MyHandler myHandler;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        HandlerThread handlerThread = new HandlerThread("handlerThread", Process.THREAD_PRIORITY_DEFAULT);
        handlerThread.start();
        myHandler = new MyHandler(handlerThread.getLooper());
        //myHandler = new MyHandler(getMainLooper());
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        myHandler.sendEmptyMessage(MY_MSG);

        return START_NOT_STICKY;
    }

    private class MyHandler extends Handler {
        private MyHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MY_MSG:
                    Log.i(TAG, "handleMessage:" + msg.what);
            }
        }
    }
}
